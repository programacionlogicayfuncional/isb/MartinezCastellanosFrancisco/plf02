(ns plf02.core)


;PREDICADOS
;associative?
(defn función-associative?-1
  [a b c]
  (associative? [a b c]))

(defn función-associative?-2
  [c d e]
  (associative? #{c d e}))

(defn función-associative?-3
  [f g]
  (associative? {f g}))

(función-associative?-1 2 3 4)
(función-associative?-2 4 5 6)
(función-associative?-3 1 3)

;boolean?

(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [b]
  (boolean? (new Boolean b)))

(defn función-boolean?-3
  [c]
  (boolean? c))


(función-boolean?-1 0)
(función-boolean?-2 false)
(función-boolean?-3 nil)

;char

(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [b c a]
  (char? #{b c a}))

(defn función-char?-3
  [c]
  (char? (last c)))

(función-char?-1 'a')
(función-char?-2 "b" "c" "a")
(función-char?-3 "a")

;coll

(defn función-coll?-1
  [a]
  (coll? (last a)))
(defn función-coll?-2
  [b]
  (coll? '(b)))
(defn función-coll?-3
  [c]
  (coll? c))

(función-coll?-1 [])
(función-coll?-2 '(1 2 3))
(función-coll?-3 nil)

;decimal

(defn función-decimal?-1
  [a]
  (decimal? a))
(defn función-decimal?-2
  [b]
  (decimal? b))
(defn función-decimal?-3
  [c]
  (decimal? c))

(función-decimal?-1 4.0)
(función-decimal?-2 1.5M)
(función-decimal?-3 10009999)

;double

(defn función-double?-1
  [a]
  (double? '(a)))

(defn función-double?-2
  [b]
  (double? b))

(defn función-double?-3
  [c]
  (double? (new Double c)))

(función-double?-1  10)
(función-double?-2  2.20)
(función-double?-3  1.00000000)

;float

(defn función-float?-1
  [a]
  (float? (new Float a)))

(defn función-float?-2
  [b]
  (float? b))

(defn función-float?-3
  [c]
  (float? c))

(función-float?-1 "1.0")
(función-float?-2 2)
(función-float?-3 1.0)

;ident?

(defn función-ident?-1
  [a]
  (ident? a))
(defn función-ident?-2
  [b]
  (ident? b))
(defn función-ident?-3
  [c]
  (ident? c))

(función-ident?-1 'hi)
(función-ident?-2 "test")
(función-ident?-3  :10101)

;indexed?

(defn función-indexed?-1
  [a ]
  (indexed? a ))

(defn función-indexed?-2
[a  b]
  (indexed?  #{a b }))

(defn función-indexed?-3
  [a b]
  (indexed? { a b}))

(función-indexed?-1  1 )
(función-indexed?-2  1 2)
(función-indexed?-3  1 2 )

;int
(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [b]
  (int? (bigint b)))

(defn función-int?-3
  [c]
  (int? (new Integer c)))

(función-int?-1 95)
(función-int?-2 104)
(función-int?-3 -90)

;integer

(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [b]
  (integer? (inc  b)))

(defn función-integer?-3
  [c]
  (integer? (dec c)))

(función-integer?-1 6)
(función-integer?-2 -112.0)
(función-integer?-3 3)

;keyword

(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [b]
  (keyword? (new String b)))

(defn función-keyword?-3
  [c]
  (keyword? (new Integer c)))

(función-keyword?-1 'c')
(función-keyword?-2 "HI")
(función-keyword?-3 9)

;list
(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [b]
  (list? (first b)))

(defn función-list?-3
  [c]
  (list? (last c)))

(función-list?-1 10)
(función-list?-2 '(1 2 3 4))
(función-list?-3 [ 2 4 6])

;map-entry?

(defn función-map-entry?-1
  [a b c d]
  (map-entry? {a b c d}))

(defn función-map-entry?-2
  [a b]
  (map-entry? (last {a b})))

(defn función-map-entry?-3
  [a b]
  (map-entry? #{a b}))

(función-map-entry?-1  2 3 4 5)
(función-map-entry?-2   "a" "b")
(función-map-entry?-3 1 2)

;map?

(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a b]
  (map? (hash-map a b)))

(defn función-map?-3
  [a b]
  (map? #{ a b }))

(función-map?-1 3)
(función-map?-2 nil 1)
(función-map?-3  3 4 )

; nat-int

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [b]
  (nat-int? b))

(defn función-nat-int?-3
  [-c]
  (nat-int? -c))

(función-nat-int?-1 -3)
(función-nat-int?-2 923)
(función-nat-int?-3 -2)

;number
(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a b]
  (number? [a b]))

(defn función-number?-3
  [a b c]
  (map number? [a b c]))

(función-number?-1 1)
(función-number?-2 30 40.1)
(función-number?-3 nil nil nil)

;pos-int

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [b]
  (pos-int? b))

(defn función-pos-int?-3
  [c]
  (pos-int? c))

(función-pos-int?-1 0.9)
(función-pos-int?-2 -9)
(función-pos-int?-3  100)

;ratio

(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [b]
  (ratio? b))

(defn función-ratio?-3
  [a b]
  (ratio? [a b]))

(función-ratio?-1 0.0001)
(función-ratio?-2  6)
(función-ratio?-3 1/2 1/2)

;rational

(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [b]
  (rational? b))

(defn función-rational?-3
  [c]
  (rational? c))

(función-rational?-1 -0.01)
(función-rational?-2 1/2)
(función-rational?-3 -1/2)

;seq?

(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a b]
  (seq? [a b]))

(defn función-seq?-3
  [a b c]
  (seq? [a b c]))

(función-seq?-1 nil)
(función-seq?-2 10 20)
(función-seq?-3  10 21 34)

;seqable?

(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a b]
  (seqable? [a b]))

(defn función-seqable?-3
  [a b c]
  (seqable? [a b c]))

(función-seqable?-1 nil)
(función-seqable?-2 5 0)
(función-seqable?-3 4 3.1 2)

;sequential

(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a b]
  (sequential? [a b]))

(defn función-sequential?-3
  [a b ]
  (sequential? (range a b)))

(función-sequential?-1 nil)
(función-sequential?-2  4 8)
(función-sequential?-3   1.1 -1)

;set

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a b]
  (set? #(a b)))

(defn función-set?-3
  [a b c d] 
  (set? '(a b c d)))

(función-set?-1 0)
(función-set?-2 59 90)
(función-set?-3 1 2 3 1)

;some


(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a b]
  (some? [a b]))

(defn función-some?-3
  [a b c]
  (some? [a b c]))

(función-some?-1 nil)
(función-some?-2 1 2)
(función-some?-3 3 4 5)

;string

(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a b]
  (string? [a b]))

(defn función-string?-3
  [a b c]
  (string? [a b c]))

(función-string?-1  "000")
(función-string?-2  1.2 -1.2)
(función-string?-3 "loli" "lolo" "lolu")

;symbol
(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a b]
  (symbol? [a b]))

(defn función-symbol?-3
  [a b c]
  (symbol? #{a b c}))

(función-symbol?-1  "k")
(función-symbol?-2   1 2)
(función-symbol?-3    3  4 5)

;vector

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a b]
  (vector? [a b]))

(defn función-vector?-3
  [a b]
  (vector? {a b}))

(función-vector?-1 '[-56])
(función-vector?-2  1 2)
(función-vector?-3  0 0)

;funciones de orden superior

;drop
(defn función-drop-1
[a b c d]
  (drop a [a b c d]))

(defn función-drop-2
  [a b c ]
  (take a (drop a (range a b c ))))

(defn función-drop-3
  [a ]
  (drop a [a]))

(función-drop-1 2 3 4 5)
(función-drop-2 3 10 0)
(función-drop-3 10)

;drop last
(defn función-drop-last-1
  [a]
  (drop-last [a]))

(defn función-drop-last-2
  [a b]
  (drop-last [a b]))

(defn función-drop-last-3
  [a b c ]
  (drop-last [a b c ]))

(función-drop-last-1 0)
(función-drop-last-2 100 45)
(función-drop-last-3 "Z" "Y" "X")

;drop while

(defn función-drop-while-1
  [a]
  (drop-while a #{1 3 4 5}))

(defn función-drop-while-2
  [a]
  (drop-while a [1 2 3 4]))

(defn función-drop-while-3
  [a]
  (drop-while a '( 15 53 12 1)))


(función-drop-while-1 #(> 3 %))
(función-drop-while-2 #(>= 3 %))
(función-drop-while-3 #(< 3 %))

;every

(defn función-every?-1
  [a b c d]
  (every? #{a b} [c d]))

(defn función-every?-2
  [a b c d]
  (every? #{a b} [c d]))

(defn función-every?-3
  [a b c d]
  (every? #{a b} [c d]))

(función-every?-1 2 3 6 8)
(función-every?-2 14 12 10 8)
(función-every?-3 16 18 20 22)

;filterv


(defn función-filterv-1
  [a b c]
  (filterv c [a b]))

(defn función-filterv-2
  [a b c]
  (filterv c [a b]))

(defn función-filterv-3
  [a b c]
  (filterv c [a b]))

(función-filterv-1  "a" "b"<)
(función-filterv-2  3 4 >)
(función-filterv-3   01 02 <=)



;group

(defn función-group-by-1
  [a b c]
  (group-by c [a b]))

(defn función-group-by-2
  [a b c]
  (group-by c [a b]))

(defn función-group-by-3
  [a b c]
  (group-by c [a b]))

(función-group-by-1  "soy " "caos" <)
(función-group-by-2  "kill me"  "please">)
(función-group-by-3 -1 100 >=)

;iterate


(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (iterate a b))

(defn función-iterate-3
  [a b]
  (iterate a b))

(función-iterate-1 dec 10)
(función-iterate-2 inc -20)
(función-iterate-3 dec' 30)

;keep

(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))

(función-keep-1 {:a 10} [:a :b])
(función-keep-2 {:a 20} [:b :a])
(función-keep-3 {:b 30} [:a :a])

;keep-indexed

(defn función-keep-indexed-1
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b]
  (keep-indexed a b))

(función-keep-indexed-1 #(if (pos? %2) %1) [1 2])
(función-keep-indexed-2 #(if (pos? %2) %1) [4 5])
(función-keep-indexed-3 #(if (pos? %2) %1) [6 7])

;map indexed

(defn función-map-indexed-1
  [a b]
  (map-indexed a b))


(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

(función-map-indexed-1 vector "test")

(función-map-indexed-3 vector "0987655")


;map cat


(defn función-mapcat-2
  [a b]
  (mapcat a b))

 (defn función-mapcat-3
   [a b]
   (mapcat a b))


(función-mapcat-2 reverse [[2] [3]])
(función-mapcat-3 reverse [[4] [5]])



;mapv

(defn función-mapv-1
  [a b]
  (mapv a b))

(defn función-mapv-2
  [a b]
  (mapv a b))

(defn función-mapv-3
  [a b]
  (mapv a b))

(función-mapv-1 inc [0])
(función-mapv-2 dec [1])
(función-mapv-3 inc [nil])

;merge-with

(defn función-merge-with-1
  [a b c]
  (merge-with c b a))

(defn función-merge-with-2
  [a b c]
  (merge-with c b a))

(defn función-merge-with-3
  [a b c]
  (merge-with c b a))

(función-merge-with-1 {:a 1} {:b 2} +)
(función-merge-with-2 {:a 3} {:b 4} +)
(función-merge-with-3 {:a 5} {:b 6} +)


;not any

(defn función-not-any?-1
  [a b]
  (not-any? a b))

(defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a b))

(función-not-any?-1 nil? [true true true])
(función-not-any?-2 nil? [true false true])
(función-not-any?-3 nil? [true true false])

;not every

(defn función-not-every?-1
  [a b]
  (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))

(defn función-not-every?-3
  [a b]
  (not-every? a b))

(función-not-every?-1 odd? (vector 10 20))
(función-not-every?-2 odd? (vector 30 40))
(función-not-every?-3 odd? (vector 50 60))


;partition by
(defn función-partition-by-1
  [a b]
  (partition-by a b))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))

(función-partition-by-1 #(= 3 %) [10])
(función-partition-by-2 #(= 2 %) [5])
(función-partition-by-3 #(= 1 %) [2])



;reduce-kv

(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1 #(assoc %1 %3 %2) {} {:a 10})
(función-reduce-kv-2 #(assoc %1 %2 %3) {} {:a 20})
(función-reduce-kv-3 #(assoc %1 %3 %2) {} {:a 30})


;remove


(defn función-remove-1
  [a b]
  (remove a b))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a b]
  (remove a b))

(función-remove-1 pos? [2 3])
(función-remove-2 pos? [4 5])
(función-remove-3 pos? [6 7])



;reverse

(defn función-reverse-1
  [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 (vector 2 4))
(función-reverse-2 (vector 10 5))
(función-reverse-3 (vector 10 20))




;some

(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
  (some a b))

(defn función-some-3
  [a b]
  (some a b))

(función-some-1 even? (vector 2 4))
(función-some-2 even? (vector 1))
(función-some-3 even? (vector  20 30))

;sort by

(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b]
  (sort-by a b))

(función-sort-by-1 first [[ 3 4] [ 4 5]])
(función-sort-by-2 second [[5 6] [6 7]])
(función-sort-by-3 last [[7 8] [8 9]])

;split-with

(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with a b))

(defn función-split-with-3
  [a b]
  (split-with a b))

(función-split-with-1 (partial >= 1) [2 1])
(función-split-with-2 (partial >= 2) [4 5])
(función-split-with-3 (partial >= 3) [6 7])

;take

(defn función-take-1
  [a]
  (take 1 a))

(defn función-take-2
  [b]
  (take 2 b))

(defn función-take-3
  [c]
  (take 3 c))

(función-take-1 [0 1 2])
(función-take-2 [3 4 5])
(función-take-3 [6 7 8])

;take-last

(defn función-take-last-1
  [a b]
  (take-last a b))

(defn función-take-last-2
  [a b]
  (take-last a b))

(defn función-take-last-3
  [a b]
  (take-last a b))

(función-take-last-1 2 [4 8])
(función-take-last-2 6 [5 2 5])
(función-take-last-3 12 [0 1 2 3])







;take-nth

(defn función-take-nth-1
  [a b]
  (take-nth a b))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [a b]
  (take-nth a b))

(función-take-nth-1 10 (range 10))
(función-take-nth-2 20 (range 25))
(función-take-nth-3 30 (range 35))




;take-while
(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [a b]
  (take-while a b))

(defn función-take-while-3
  [a b]
  (take-while a b))

(función-take-while-1 (partial > 3) (iterate inc 0))
(función-take-while-2 (partial > 4) (iterate inc 10))
(función-take-while-3 (partial > 5) (iterate inc 20))


;update

(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c]
  (update a b c))

(defn función-update-3
  [a b c]
  (update a b c))

(función-update-1 [10 20 32] 0 inc)
(función-update-2 [3 1 2] 1 dec)
(función-update-3 [1 2 1] 2 inc)

;update in

(defn función-update-in-1
  [a b c d e]
  (update-in a b c d e))

(defn función-update-in-2
  [a b c d e]
  (update-in a b c d e))

(defn función-update-in-3
  [a b c d e]
  (update-in a b c d e))

(función-update-in-1 {:a 8} [:a] / 3 1)
(función-update-in-2 {:b 5} [:b] / 1 2)
(función-update-in-3 {:c 2} [:c] / 2 1)



